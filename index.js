const { readdirSync, writeFileSync } = require("fs");
require("dotenv").config();
async function main() {
  const paths = readdirSync("./scripts");
  for (const path of paths) {
    console.log(`Running ${path}...`)
    const { main } = require(`./scripts/${path}`);
    const output = await main();
    writeFileSync(`./data/${path}.output.json`, JSON.stringify(output, null, 2));
    console.log(`Done running ${path} output saved to ./data/${path}.output.json`);
  }
}

main();
