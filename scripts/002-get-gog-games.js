
const main = async () => {
    const gogCookie = process.env.GOG_COOKIE;
    const url = `https://api.steampowered.com/IPlayerService/GetOwnedGames/v00001/?key=${steamApiKey}&steamid=${steamId}&include_appinfo=true&format=json`;
    const result = await fetch(url)
    .then(res => res.json());
    return result;
};

module.exports = { main };
