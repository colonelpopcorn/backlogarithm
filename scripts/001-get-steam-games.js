const main = async () => {
    const steamApiKey = process.env.STEAM_API_KEY;
    const steamId = process.env.STEAM_ID;
    const url = `https://api.steampowered.com/IPlayerService/GetOwnedGames/v00001/?key=${steamApiKey}&steamid=${steamId}&include_appinfo=true&format=json`;
    const result = await fetch(url)
    .then(res => res.json());
    return result;
};

module.exports = { main };
